package com.bookshop.controller;

import com.bookshop.dto.BookDto;
import com.bookshop.entity.Book;
import com.bookshop.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.notFound;


@RestController
@RequiredArgsConstructor
@RequestMapping("/books")
public class BookController {

    private final BookService bookService;

    @GetMapping
    @ResponseBody
    public List<BookDto> findAll(){
        return bookService.findAll();
    }

    @GetMapping("/{id}")
    public BookDto findById(@PathVariable("id") Long id){
        return bookService.findById(id);
    }
//
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BookDto createBook(@RequestBody BookDto BookDto){
        return bookService.createBook(BookDto);
    }
//
    @PutMapping("/{id}")
    public BookDto updateBook(@PathVariable("id") Long id,
                              @RequestBody BookDto bookDto){
        return bookService.updateBook(id, bookDto);
    }

    @DeleteMapping("/{id}")
    public BookDto deleteBook(@PathVariable("id") Long id){
        return bookService.deleteBook(id);
    }
}
