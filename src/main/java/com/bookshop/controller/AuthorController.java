package com.bookshop.controller;

import com.bookshop.dto.AuthorDto;
import com.bookshop.dto.BookDto;
import com.bookshop.entity.Author;
import com.bookshop.service.AuthorService;
import com.bookshop.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/author")
public class AuthorController {

    private final AuthorService authorService;

    @GetMapping
    @ResponseBody
    public List<AuthorDto> findAll(){
        return authorService.findAll();
    }

    @GetMapping("/{id}")
    public AuthorDto findById(@PathVariable("id") Long id){
        return authorService.findById(id);
    }
//
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AuthorDto createAuthor(@RequestBody AuthorDto authorDto){
        return authorService.createAuthor(authorDto);
    }

}
