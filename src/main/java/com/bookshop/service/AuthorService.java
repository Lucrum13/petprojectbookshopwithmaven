package com.bookshop.service;


import com.bookshop.dto.AuthorDto;
import com.bookshop.dto.BookDto;
import com.bookshop.entity.Author;
import com.bookshop.entity.Book;
import com.bookshop.mapper.AuthorMapper;
import com.bookshop.mapper.BookMapper;
import com.bookshop.repository.AuthorRepository;
import com.bookshop.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.AbstractRequestAttributes;

import java.util.List;


@Service
@RequiredArgsConstructor
public class AuthorService {

    private final AuthorRepository authorRepository;

    private final AuthorMapper authorMapper;

    public List<AuthorDto> findAll(){
        List<Author> listOfBook = authorRepository.findAll();
        return listOfBook.stream().
                map(entity -> authorMapper.toDto(entity)).toList();
    }

    public AuthorDto findById(Long id){
        return authorMapper.toDto(authorRepository.findById(id));
    }
//
    @Transactional
    public AuthorDto createAuthor(AuthorDto authorDto){

        String[] dividedAuthorName = authorDto.getAuthorName().split(" ");

        Author author = new Author();
        author.setFirstName(dividedAuthorName[0]);
        author.setLastName(dividedAuthorName[1]);
        author.setBooks(null);

        author = authorRepository.save(author);
        return authorMapper.toDto(author);
    }

}
