package com.bookshop.service;


import com.bookshop.dto.AuthorDto;
import com.bookshop.dto.BookDto;
import com.bookshop.entity.Author;
import com.bookshop.entity.Book;
import com.bookshop.mapper.BookMapper;
import com.bookshop.repository.AuthorRepository;
import com.bookshop.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class BookService {
    private final BookRepository bookRepository;

    private final AuthorRepository authorRepository;

    private final BookMapper bookMapper;

    public List<BookDto> findAll(){
        List<Book> listOfBook = bookRepository.findAll();
        return listOfBook.stream().
                map(entity -> bookMapper.toDto(entity)).toList();
    }

    public BookDto findById(Long id){
        return bookMapper.toDto(bookRepository.findById(id));
    }
//
    @Transactional
    public BookDto createBook(BookDto bookDto){
        Book book = bookMapper.toBook(bookDto);
        Author author = authorRepository.findById(bookDto.getAuthorId());
        if(author!=null){
            book.setAuthor(author);
            book = bookRepository.save(book);
            return bookMapper.toDto(book);
        }
        return bookDto;
    }
////
    @Transactional
    public BookDto updateBook(Long id, BookDto bookDto){
        Book book = null;
        Author authorFromDto = authorRepository.findById(bookDto.getAuthorId());
        if(bookRepository.findById(id) != null &&
                authorFromDto!=null) {
            book = bookMapper.toBook(bookDto);
            book.setAuthor(authorFromDto);
        book = bookRepository.save(book);

          return bookDto;
        }
        System.out.println();
        return bookDto;
    }
////
    @Transactional
    public BookDto deleteBook(Long id){
        Book book = bookRepository.findById(id);
        if (book != null){
            bookRepository.delete(book);
            return bookMapper.toDto(book);
        }
        return bookMapper.toDto(book);
    }

}
