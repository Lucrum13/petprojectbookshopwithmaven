package com.bookshop.dto;

import com.bookshop.entity.Book;
import jakarta.persistence.Transient;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Value
public class AuthorDto {

    Long id;

    String authorName;

    List<Long> bookIds = new ArrayList<>();
}
