package com.bookshop.dto;

import com.bookshop.entity.Cover;
import lombok.*;


@Value
public class BookDto {

    Long id;

    String name;

    String brand;

    Cover cover;

    Long authorId;

    Integer count;
}
