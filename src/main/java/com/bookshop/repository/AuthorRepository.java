package com.bookshop.repository;

import com.bookshop.entity.Author;
import com.bookshop.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Integer> {


        List<Author> findAll();
        Author findById(Long id);
}
