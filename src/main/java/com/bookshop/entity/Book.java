package com.bookshop.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.io.Serializable;


@Entity
@Builder
@Setter
@Getter
@RequiredArgsConstructor
@AllArgsConstructor
@Table(name = "books")
public class Book implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String brand;

    @Enumerated(value = EnumType.STRING)
    private Cover cover;

    @ManyToOne
    @JoinColumn(name="author")
    private Author author;

    private Integer count;
}
