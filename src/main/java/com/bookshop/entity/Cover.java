package com.bookshop.entity;

public enum Cover {
    SOFT, SOLID
}
