package com.bookshop.mapper;


import com.bookshop.dto.BookDto;
import com.bookshop.entity.Author;
import com.bookshop.entity.Book;
import com.bookshop.repository.AuthorRepository;
import com.bookshop.repository.BookRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public abstract class BookMapper {

    @Mapping(target = "author", ignore = true)
    public abstract Book toBook(BookDto bookDto);

    @Mapping(source = "author", target = "authorId", qualifiedByName = "mapToDto")
    public abstract BookDto toDto(Book book);

    @Named("mapToDto")
    public static Long mapToDto(Author author){
        return author.getId();
    };

}
