package com.bookshop.mapper;


import com.bookshop.dto.AuthorDto;
import com.bookshop.dto.BookDto;
import com.bookshop.entity.Author;
import com.bookshop.entity.Book;
import com.bookshop.repository.BookRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.stream.Collectors;

@Mapper
public abstract class AuthorMapper{

    @Mapping(target = "books", ignore = true)
    public abstract Author toAuthor(AuthorDto dto);

    @Mapping(target = "authorName",expression = "java(entity.getFirstName() + \" \" + " +
            "entity.getLastName())")
//    @Mapping(target = "bookIds", source = "books")
    @Mapping(target = "bookIds", source = "books", qualifiedByName = "map")
    public abstract AuthorDto toDto(Author entity);

    @Named("map")
    public static List<Long> map(List<Book> books){
        if (books != null ){
            return books.stream().map(Book::getId).toList();
        }
        return null;
    };

}
